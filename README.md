# Remove Meta Info

This module enables the user to Remove the META tag info from the head of the Drupal site, and it works in both Drupal versions 8, 9 or 10 as follows:

- Remove Drupal generated META tag from head
- Removes only <meta name="Generator" content="Drupal 8, 9, 10" />
- Lightweight with only one PHP file
- No external dependencies
- Has been tested in Drupal 9 site

# Versions

- Use older version 1.0.x or 1.0.1 for Drupal 8 and 9
- Use latest version 1.0.2 for Drupal 10
